package test;

using Lambda;

import utest.Assert; 

using stipple.Template;
import stipple.AST; 
import stipple.Helpers;
import stipple.Lexer;

class HelpersTest {
	public function new () {}

	function testHelperWithComplexLookup () {

		Assert.same("<a href='/root/goodbye'>Goodbye</a>", new HelperWithComplexLookup ().fromString("{{#goodbyes}}{{{link ../prefix}}}{{/goodbyes}}").execute({ prefix: "/root", goodbyes: [{ text: "Goodbye", url: "goodbye" }]}));
	}

	function testHelperBlockWithComplexLookupExpression () {

		Assert.same("Goodbye Alan! goodbye Alan! GOODBYE Alan! ", new Template ().fromString("{{#goodbyes}}{{../name}}{{/goodbyes}}").execute({ name: "Alan", goodbyes: function (_, options) return 
			["Goodbye", "goodbye", "GOODBYE"].fold(function (byte, out: String) return out + byte + " " + options.fn(options.ctx, {}) + "! ", "") }));
	}

	function testHelperWithComplexLookupAndNestedTemplate () {

		Assert.same("<a href='/root/goodbye'>Goodbye</a>", new HelperWithComplexLookupAndNestedTemplate ().fromString("{{#goodbyes}}{{#link ../prefix}}{{text}}{{/link}}{{/goodbyes}}").execute({prefix: '/root', goodbyes: [{text: "Goodbye", url: "goodbye"}]}));
	}

	function testBlockHelper () {

		Assert.same("GOODBYE! cruel world!", new Template ().fromString("{{#goodbyes}}{{text}}! {{/goodbyes}}cruel {{world}}!").execute({world: "world", goodbyes: function (_, options) return options.fn({text: "GOODBYE"}, {})}));
	}

	function testBlockHelperStayingInTheSameContext () {

		Assert.same("<form><p>Yehuda</p></form>", new Template ().fromString("{{#form}}<p>{{name}}</p>{{/form}}").execute({ name: "Yehuda", form: function (_, options) return "<form>" + options.fn(options.ctx, {}) + "</form>" }));
	}

	function testBlockHelperShouldHaveContextInThis () {
		var data = { people: [
			{ name: "Alan", id: 1 },
			{ name: "Yehuda", id: 2 }
		]};

		Assert.same("<ul><li><a href=\"/people/1\">Alan</a></li><li><a href=\"/people/2\">Yehuda</a></li></ul>", new BlockHelperShouldHaveContextInThis ().fromString("<ul>{{#people}}<li>{{#link}}{{name}}{{/link}}</li>{{/people}}</ul>").execute(data));
	}

	function testBlockHelperForUndefinedValue () {
		Assert.same("", new Template ().fromString("{{#empty}}shouldn't render{{/empty}}").execute({}));
	}

	function testBlockHelperPassingANewContext () {
		Assert.same("<form><p>Yehuda</p></form>", new Template ().fromString("{{#form yehuda}}<p>{{name}}</p>{{/form}}").execute({yehuda: {name: "Yehuda"}, form: function (context: Array<RType>, options: HelperOptions) return switch (context) {
			case [Object(o)]: "<form>" + options.fn(o, {}) + "</form>";
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	function testBlockHelperPassingAComplexPathContext () {
		Assert.same("<form><p>Harold</p></form>", new Template ().fromString("{{#form yehuda/cat}}<p>{{name}}</p>{{/form}}").execute({yehuda: {name: "Yehuda", cat: { name: "Harold" }}, form: function (context: Array<RType>, options: HelperOptions) return switch (context) {
			case [Object(o)]: "<form>" + options.fn(o, {}) + "</form>";
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	function testNestedBlockHelpers () {

		Assert.same("<form><p>Yehuda</p><a href=\"Yehuda\">Hello</a></form>", new NestedBlockHelpers ().fromString("{{#form yehuda}}<p>{{name}}</p>{{#link}}Hello{{/link}}{{/form}}").execute({yehuda: {name: "Yehuda"}, form: function (context: Array<RType>, options: HelperOptions) return switch (context) {
			case [Object(o)]: "<form>" + options.fn(o, {}) + "</form>";
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	function testBlockHelperInvertedSections () {

		Assert.same("<ul><li>Alan</li><li>Yehuda</li></ul>", new BlockHelperInvertedSections ().fromString("{{#list people}}{{name}}{{^}}<em>Nobody's here</em>{{/list}}").execute({people: [{name: "Alan"}, {name: "Yehuda"}]}));
		Assert.same("<p><em>Nobody's here</em></p>", new BlockHelperInvertedSections ().fromString("{{#list people}}{{name}}{{^}}<em>Nobody's here</em>{{/list}}").execute({people: []}));
		Assert.same("<p>Nobody&#039;s here</p>", new BlockHelperInvertedSections ().fromString("{{#list people}}Hello{{^}}{{message}}{{/list}}").execute({ people: [], message: "Nobody's here" }));
	}

	function testHelpersHashProvidingAHelpersHash () {
		Assert.same("Goodbye cruel world!", new HelpersHashProvidingAHelpersHash ().fromString("Goodbye {{cruel}} {{world}}!").execute({cruel: "cruel"}));

		Assert.same("Goodbye cruel world!", new HelpersHashProvidingAHelpersHash ().fromString("Goodbye {{#iter}}{{cruel}} {{world}}{{/iter}}!").execute({ iter: [{cruel: "cruel"}]}));
	}

	function testHelpersHashInCasesOfConflictHelpersWin () {
		Assert.same("helpers", new HelpersHashInCasesOfConflictHelpersWin ().fromString("{{{lookup}}}").execute({lookup: "Explicit"}));
		Assert.same("helpers", new HelpersHashInCasesOfConflictHelpersWin ().fromString("{{lookup}}").execute({lookup: "Explicit"}));
	}

	function testHelpersHashIsAvailableInNestedContexts () {
		Assert.same("helper", new HelpersHashIsAvailableInNestedContexts ().fromString("{{#outer}}{{#inner}}{{helper}}{{/inner}}{{/outer}}").execute({ outer: { inner: { unused: [] } } }));
	}

	//function testHelpersHashShouldAugmentTheGlobalHash () {
	//	Template.registerHelper("test_helper", function (_, o) return "found it!");

	//	Assert.same("found it! Goodbye cruel world!!", new Template ().fromString("{{test_helper}} {{#if cruel}}Goodbye {{cruel}} {{world}}!{{/if}}").execute({cruel: "cruel"}, { world: function (_, o) return "world!" }));

	//	Template.resetHelpers();
	//}

	//function testMultipleGlobalHelperRegistration () {
	//	Template.registerHelper("if", Helpers.all().get("if"));
	//	Template.registerHelper("world", function (_, o) return "world!");
	//	Template.registerHelper("test_helper", function (_, o) return "found it!");

	//	Assert.same("found it! Goodbye cruel world!!", new Template ().fromString("{{test_helper}} {{#if cruel}}Goodbye {{cruel}} {{world}}!{{/if}}").execute({cruel: "cruel"}));

	//	Template.resetHelpers();
	//}

	function testDecimalNumberLiterals () {

		Assert.same("Message: Hello -1.2 1.2 times", new Template ().fromString("Message: {{hello -1.2 1.2}}").execute({ hello: function (_: Array<RType>, opts: HelperOptions) return switch (_) {
			case [Const(num), Const(num2)]: 'Hello ${num} ${num2} times';
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	function testNegativeNumberLiterals () {

		Assert.same("Message: Hello -12 times", new Template ().fromString("Message: {{hello -12}}").execute({hello: function (times: Array<RType>, o: HelperOptions) return switch (times) {
			case [Const(num)]: 'Hello ${num} times';
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	function testStringLiteralParametersSimpleLiterals () {
		Assert.same("Message: Hello world 12 times: true false", new Template ().fromString('Message: {{hello "world" 12 true false}}').execute({hello: function (_: Array<RType>, opts: HelperOptions) return switch (_) {
			case [Const(param), Const(times), Boolean(bool1), Boolean(bool2)]:
				'Hello ${param} ${times} times: ${bool1} ${bool2}';
			case v: throw 'invalid object returned to helper: ${v}';
		}}));
	}

	/** Problem introduced in the hxparse transition.
	function testStringLiteralParametersUsingAQuoteRaisesAnError () {
trace (new HpsParser(byte.ByteData.ofString('Message: {{hello wo"rld"}}')).parse());
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString('Message: {{hello wo"rld"}}')).parse()), hxparse.Unexpected);
	}
	*/

	function testStringLiteralParametersEscapingAString () {
		Assert.same("Message: Hello \"world\"", new Template ().fromString('Message: {{{hello "\\"world\\""}}}').execute({ hello: function (_: Array<RType>, opts: HelperOptions) return 'Hello ${_.extract()}' }));
	}

	function testStringLiteralParametersWithSingleQuotes () {
		Assert.same("Message: Hello Alan's world", new Template ().fromString('Message: {{{hello "Alan\'s world"}}}').execute({ hello: function (_: Array<RType>, opts: HelperOptions) return 'Hello ${_.extract()}'}));
	}

	function testMultipleParametersSimple () {

		Assert.same("Message: Goodbye cruel world", new Template ().fromString("Message: {{goodbye cruel world}}").execute({cruel: "cruel", world: "world", goodbye: function (_: Array<RType>, o: HelperOptions) return 'Goodbye ${_.extract(" ")}'}));
	}

	function testMultipleParametersBlock () {
		Assert.same("Message: Goodbye cruel world", new Template ().fromString('Message: {{#goodbye cruel world}}{{greeting}} {{adj}} {{noun}}{{/goodbye}}').execute({cruel: "cruel", world: "world", goodbye: function (_:Array<RType>, options: HelperOptions) return switch (_) { 
			case [cruel, world]:
				options.fn({ greeting: "Goodbye", adj: [cruel].extract(), noun: [world].extract() }, {});
			case v: throw "invalid number of arguments called to helper: " + v;
		}}));
	}

	function testHashHelpersCanTakeAnOptionalHash () {

		Assert.same("GOODBYE CRUEL WORLD 12 TIMES", new Template ().fromString('{{goodbye cruel="CRUEL" world="WORLD" times=12}}').execute({goodbye: function (_: Array<RType>, options: HelperOptions) 
			return 'GOODBYE ${[options.hash.cruel, options.hash.world, options.hash.times].extract(" ")} TIMES'}));
	}

	function testHashHelpersCanTakeAnOptionsHashWithBooleans () {
		var hash = { 
			goodbye: function (_: Array<RType>, options: HelperOptions) return switch (options.hash.print) {
				case Boolean(true): 'GOODBYE ${[options.hash.cruel, options.hash.world].extract(" ")}';
				case Boolean(false): 'NOT PRINTING';
				case _: 'THIS SHOULD NOT HAPPEN';
			}
		}
		Assert.same("GOODBYE CRUEL WORLD", new Template ().fromString('{{goodbye cruel="CRUEL" world="WORLD" print=true}}').execute(hash));
		Assert.same("NOT PRINTING", new Template ().fromString('{{goodbye cruel="CRUEL" world="WORLD" print=false}}').execute(hash));
	}

	function testHashBlockHelpersCanTakeAnOptionalHash () {

		Assert.same("GOODBYE CRUEL world 12 TIMES", new Template ().fromString('{{#goodbye cruel="CRUEL" times=12}}world{{/goodbye}}').execute({ goodbye: function (_: Array<RType>, options: HelperOptions) return 'GOODBYE ${[options.hash.cruel].extract()} ${options.fn(options.ctx, {})} ${[options.hash.times].extract()} TIMES' }));
	}

	function testHashBlockHelpersCanTakeAnOptionalHashWithSingleQuotedStrings () {

		Assert.same("GOODBYE CRUEL world 12 TIMES", new Template ().fromString("{{#goodbye cruel='CRUEL' times=12}}world{{/goodbye}}").execute({ goodbye: function (_: Array<RType>, options: HelperOptions) return 'GOODBYE ${[options.hash.cruel].extract()} ${options.fn(options.ctx, {})} ${[options.hash.times].extract()} TIMES' }));	
	}

	function testHashBlockCanTakeAnOptionalHashWithBooleans () {
		var hash = {
			goodbye: function (_: Array<RType>, options: HelperOptions) return switch (options.hash.print) {
				case Boolean(true): 
					'GOODBYE ${[options.hash.cruel].extract()} ${options.fn(options.ctx, {})}';
				case Boolean(false): 'NOT PRINTING';
				case _: 'THIS SHOULD NOT HAPPEN';
			}
		};

		Assert.same("GOODBYE CRUEL world", new Template ().fromString('{{#goodbye cruel="CRUEL" print=true}}world{{/goodbye}}').execute(hash));
		Assert.same("NOT PRINTING", new Template ().fromString('{{#goodbye cruel="CRUEL" print=false}}world{{/goodbye}}').execute(hash));
	}

	// Note: Won't implement (wants to throw in helper_missing, but not in block_helper_missing?)
	//function testHelperMissingContextRaiseException () {
	//	Assert.raises(function () new Template ().fromString("{{hello}} {{link_to world}}").execute({}), String);
	//}

	function testHelperMissingContextCustomHelperUsed () {
		Assert.same("Hello <a>world</a>", new HelperMissingContextCustomHelperUsed ().fromString("{{hello}} {{link_to world}}").execute({ hello: "Hello", world: "world" }));
	}

	function testNameFieldShouldIncludeInAmbiguousMustacheCalls () {
		Assert.same("ran: helper", new NameFieldHelpers ().fromString('{{helper}}').execute({}));
	}

	function testNameFieldShouldIncludeInHelperMustacheCalls () {
		Assert.same("ran: helper", new NameFieldHelpers ().fromString('{{helper 1}}').execute({}));
	}

	function testNameFieldShouldIncludeInAmbiguousBlockCalls () {
		Assert.same("ran: helper", new NameFieldHelpers ().fromString('{{#helper}}{{/helper}}').execute({}));
	}

	function testNameFieldShouldIncludeInSimpleBlockCalls () {
		Assert.same("missing: ./helper", new NameFieldHelpers ().fromString('{{#./helper}}{{/./helper}}').execute({}));
	}

	function testNameFieldShouldIncludeInHelperBlockCalls () {
		Assert.same("ran: helper", new NameFieldHelpers ().fromString('{{#helper 1}}{{/helper}}').execute({}));
	}

	function testNameFieldShouldIncludeFullId () {
		Assert.same("missing: foo.helper", new NameFieldHelpers ().fromString('{{#foo.helper}}{{/foo.helper}}').execute({ foo: {} }));
	}

	function testNameConflictsHelpersTakePrecedenceOverSameNamedContextProperties () {

		Assert.same("GOODBYE cruel WORLD", new NameConflictsHelpersTakePrecedenceOverSameNamedContextProperties ().fromString("{{goodbye}} {{cruel world}}").execute({ goodbye: "goodbye", world: "world" }));
	}

	function testNameConflictsHelpersTakePrecedenceOverSameNamedContextPropertiesInNestedContext () {

		Assert.same("GOODBYE cruel WORLD", new NameConflictsHelpersTakePrecedenceOverSameNamedContextPropertiesInNestedContext ().fromString("{{#goodbye}} {{cruel world}}{{/goodbye}}").execute({ goodbye: "goodbye", world: "world" }));
	}

	function testNameConflictsScopedNamesTakePrecendenceOverHelpers () {
		Assert.same("goodbye cruel WORLD cruel GOODBYE", new NameConflictsHelpersTakePrecedenceOverSameNamedContextProperties ().fromString("{{this.goodbye}} {{cruel world}} {{cruel this.goodbye}}").execute({ goodbye: "goodbye", world: "world" }));
	}
}

class HelperWithComplexLookup<T> extends Template<T> {
	override function localHelpers(): Core return mergeHelpers({
		link: function (_: Array<RType>, options: HelperOptions) return switch (_) {
			case [Const(prefix)]: "<a href='" + prefix + "/" + options.ctx.url + "'>" + options.ctx.text + "</a>";
			case _: throw 'invalid object returned to helper: ${_}';
		}
	}, Helpers.core());
}

class HelperWithComplexLookupAndNestedTemplate<T> extends Template<T> {
	override function localHelpers(): Core return mergeHelpers({
		link: function (_: Array<RType>, options: HelperOptionsTyped<{text: String, url: String}, {}, {}>) return switch (_) {
			case [Const(prefix)]: "<a href='" + prefix + "/" + options.ctx.url + "'>" + options.fn(options.ctx, {}) + "</a>";
			case _: throw 'invalid object returned to helper: ${_}';
		}
	}, Helpers.core());
}

class BlockHelperShouldHaveContextInThis<T> extends Template<T> {
	override function localHelpers(): Core return mergeHelpers({
		link: function (_: Array<RType>, options: HelperOptionsTyped<{name: String, id: Int}, {}, {}>) return '<a href="/people/${options.ctx.id}">${options.fn(options.ctx, {})}</a>'
	}, Helpers.core());
}

class NestedBlockHelpers<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		link: function (_: Array<RType>, options: HelperOptions) return '<a href="${options.ctx.name}">${options.fn(options.ctx, {})}</a>'
	}, Helpers.core());
}

class BlockHelperInvertedSections<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		list: function (_: Array<RType>, options: HelperOptions) {
			return switch (_) {
				case [Many(a)] if (a.length > 0): a.fold(function (ctx, out: String) return
					switch (ctx) {
						case Object(o): '${out}<li>${options.fn(o, {} ) }</li>';
						case _: '${out}<li>${options.fn([ctx].extract(), {} ) }</li>';
					}, "<ul>") + "</ul>";
				case [Many(a)] if (a.length == 0): '<p>${options.inverse(options.ctx, {})}</p>';
				case v: throw 'invalid object returned to helper: ${v}';
			}
		}
	}, Helpers.core());
}

class HelpersHashProvidingAHelpersHash<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		world: function (_, o) return "world"
	}, Helpers.core());
}

class HelpersHashInCasesOfConflictHelpersWin<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		lookup: function (_, o) return "helpers"
	}, Helpers.core());
}

class HelpersHashIsAvailableInNestedContexts<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		helper: function (_, o) return "helper"
	}, Helpers.core());
}

class HelperMissingContextCustomHelperUsed<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		helperMissing: function (_: Array<RType>, options: HelperOptions) return switch (_) {
			case [mesg] if (options.name == "link_to"): new SafeString('<a>${[mesg].extract()}</a>').toString();
			case _: throw "invalid arguments passed to helper";
		}
	}, Helpers.core());
}

class NameFieldHelpers<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		helperMissing: function (_: Array<RType>, options: HelperOptions) return 'missing: ${options.name}',
		helper: function (_: Array<RType>, options: HelperOptions) return 'ran: ${options.name}'
	}, Helpers.core());
}

class NameConflictsHelpersTakePrecedenceOverSameNamedContextProperties<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		goodbye: function (_: Array<RType>, opts: HelperOptionsTyped<{goodbye: String}, {}, {}>): String return opts.ctx.goodbye.toUpperCase(),
		cruel: function (_: Array<RType>, opts: HelperOptionsTyped<{goodbye: String}, {}, {}>): String return switch (_) {
			case [world]: 'cruel ${[world].extract().toUpperCase()}';
			case v: throw "invalid number of arguments passed to helper: " + v;
		}
	}, Helpers.core());
}

class NameConflictsHelpersTakePrecedenceOverSameNamedContextPropertiesInNestedContext<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		goodbye: function (_: Array<RType>, opts: HelperOptionsTyped<{ goodbye: String }, { goodbye: String}, {}>) return opts.ctx.goodbye.toUpperCase() + opts.fn(opts.ctx, {}),
		cruel: function (_: Array<RType>, opts: HelperOptions) return switch (_) {
			case [world]: 'cruel ${[world].extract().toUpperCase()}';
			case _: throw "invalid number of arguments passed to helper";
		}
	}, Helpers.core());
}
