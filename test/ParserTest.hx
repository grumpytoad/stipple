package test;

import utest.Assert;

import stipple.Lexer;
import stipple.AST;

class ParserTest {
	public function new () {}

	function testParsesSimpleMustaches () {
		Assert.same("{{ ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo}}")).parse())));
		Assert.same("{{ ID:foo? [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo?}}")).parse())));
		Assert.same("{{ ID:foo_ [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo_}}")).parse())));
		Assert.same("{{ ID:foo- [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo-}}")).parse())));
		Assert.same("{{ ID:foo: [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo:}}")).parse())));
	}

	function testParsesSimpleMustachesWithData () {
		Assert.same("{{ @ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{@foo}}")).parse())));
	}

	function testParsesMustachesWithPaths () {
		Assert.same("{{ PATH:foo/bar [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo/bar}}")).parse())));
	}

	function testParsesMustachesWithContextualPaths () {
		Assert.same("{{ ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{this/foo}}")).parse())));
	}

	function testParsesMustachesWithADash () {
		Assert.same("{{ ID:foo-bar [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo-bar}}")).parse())));
	}

	function testParsesMustachesWithParameters () {
		Assert.same("{{ ID:foo [ID:bar] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo bar}}")).parse())));
	}

	function testParsesMustachesWithStringParameters () {
		Assert.same('{{ ID:foo [ID:bar, "baz"] }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo bar \"baz\" }}")).parse())));
	}

	function testParsesMustachesWithIntegerParameters () {
		Assert.same("{{ ID:foo [INTEGER{1}] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo 1}}")).parse())));
	}

	function testParsesMustachesWithBooleanParameters () {
		Assert.same("{{ ID:foo [BOOLEAN{true}] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo true}}")).parse())));
		Assert.same("{{ ID:foo [BOOLEAN{false}] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo false}}")).parse())));
	}

	function testParsesMustachesWithDataParameters () {
		Assert.same("{{ ID:foo [@ID:bar] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{foo @bar}}")).parse())));
	}

	function testParsesMustachesWithHashArguments () {
		Assert.same("{{ ID:foo [] HASH{bar=ID:baz} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=baz }}")).parse())));
		Assert.same("{{ ID:foo [] HASH{bar=INTEGER{1}} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=1 }}")).parse())));
		Assert.same("{{ ID:foo [] HASH{bar=BOOLEAN{true}} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=true }}")).parse())));
		Assert.same("{{ ID:foo [] HASH{bar=BOOLEAN{false}} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=false }}")).parse())));
		Assert.same("{{ ID:foo [] HASH{bar=@ID:baz} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=@baz }}")).parse())));
		Assert.same("{{ ID:foo [] HASH{bar=ID:baz, bat=ID:bam} }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=baz bat=bam }}")).parse())));
		Assert.same('{{ ID:foo [] HASH{bar=ID:baz, bat="bam"} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bar=baz bat=\"bam\" }}")).parse())));
		Assert.same('{{ ID:foo [] HASH{bat="bam"} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo bat='bam' }}")).parse())));
		Assert.same('{{ ID:foo [ID:omg] HASH{bar=ID:baz, bat="bam"} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo omg bar=baz bat=\"bam\" }}")).parse())));
		Assert.same('{{ ID:foo [ID:omg] HASH{bar=ID:baz, bat="bam", baz=INTEGER{1}} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo omg bar=baz bat=\"bam\" baz=1 }}")).parse())));
		Assert.same('{{ ID:foo [ID:omg] HASH{bar=ID:baz, bat="bam", baz=BOOLEAN{true}} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo omg bar=baz bat=\"bam\" baz=true }}")).parse())));
		Assert.same('{{ ID:foo [ID:omg] HASH{bar=ID:baz, bat="bam", baz=BOOLEAN{false}} }}', ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{ foo omg bar=baz bat=\"bam\" baz=false }}")).parse())));
	}

	function testParsesContentsFollowedByMustache () {
		Assert.same("CONTENT[ 'foo bar ' ]{{ ID:baz [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("foo bar {{baz}}")).parse())));
	}
	
	function testParsesAPartial () {
		Assert.same("{{> PARTIAL:foo }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{> foo }}")).parse())));
	}

	function testParsesAPartialWithContext () {
		Assert.same("{{> PARTIAL:foo ID:bar }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{> foo bar}}")).parse())));
	}

	function testParsesAPartialWithAComplexName () {
		Assert.same("{{> PARTIAL:shared/partial?.bar }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{> shared/partial?.bar}}")).parse())));
	}

	function testParsesAComment () {
		Assert.same("{{! ' this is a comment ' }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{! this is a comment }}")).parse())));
	}

	function testParsesAMultiLineComment () {
		Assert.same("{{! '\nthis is a multi-line comment\n' }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{!\nthis is a multi-line comment\n}}")).parse())));
	}

	function testParsesAnInverseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} PROGRAM: CONTENT[ ' bar ' ] {{^}} CONTENT[ ' baz ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}} bar {{^}} baz {{/foo}}")).parse())));
	}

	function testParsesAnInverseElseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} PROGRAM: CONTENT[ ' bar ' ] {{^}} CONTENT[ ' baz ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}} bar {{else}} baz {{/foo}}")).parse())));
	}

	function testParsesEmptyBlocks () {
		Assert.same("BLOCK: {{ ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}}{{/foo}}")).parse())));
	}

	function testParsesEmptyBlocksWithEmptyInverseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}}{{^}}{{/foo}}")).parse())));
	}

	function testParsesEmptyBlocksWithEmptyInverseElseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }}", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}}{{else}}{{/foo}}")).parse())));
	}

	function testParsesNonEmptyBlocksWithEmptyInverseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} PROGRAM: CONTENT[ ' bar ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}} bar {{^}}{{/foo}}")).parse())));
	}

	function testParsesNonEmptyBlocksWithEmptyInverseElseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} PROGRAM: CONTENT[ ' bar ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}} bar {{else}}{{/foo}}")).parse())));
	}

	function testParsesEmptyBlocksWithNonEmptyInverseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} {{^}} CONTENT[ ' bar ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}}{{^}} bar {{/foo}}")).parse())));
	}

	function testParsesEmptyBlocksWithNonEmptyInverseElseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} {{^}} CONTENT[ ' bar ' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{#foo}}{{else}} bar {{/foo}}")).parse())));
	}

	function testParsesAStandaloneInverseSection () {
		Assert.same("BLOCK: {{ ID:foo [] }} {{^}} CONTENT[ 'bar' ]", ConsoleWriter.dump(AST.parse(new HpsParser(byte.ByteData.ofString("{{^foo}}bar{{/foo}}")).parse())));
	}

	function testRaisesOnParseError () {
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("foo{{^}}bar")).parse()), String);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{foo}")).parse()), hxparse.UnexpectedChar);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{foo &}}")).parse()), hxparse.UnexpectedChar);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{#goodbyes}}{{/hellos}}")).parse()), String);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("hello\nmy\n{{foo}")).parse()), hxparse.UnexpectedChar);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("hello\n\nmy\n\n{{foo}")).parse()), hxparse.UnexpectedChar);
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("\n\nhello\n\nmy\n\n{{foo}")).parse()), hxparse.UnexpectedChar);
	}
}
