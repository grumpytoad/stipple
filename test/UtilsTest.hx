package test;

import utest.Assert; 

import stipple.AST; 
using stipple.Template;

using Lambda;

class UtilsTest {
	public function new () {}

	function testConstructingSafeString () {
		var safe = new SafeString("testing 1, 2, 3");
		Assert.same(safe.toString(), "testing 1, 2, 3");
	}

	function testShouldNotEscapeSafeString () {
		var name = new SafeString("<em>Sean O&#x27;Malley</em>");
		Assert.same("<em>Sean O&#x27;Malley</em>", new Template ().fromString("{{name}}").execute({ name: name }));

	}

	#if sys
	function testFromFile () {
		Assert.same("hello world\n", new Template <{foo: String}>().fromFile("./test/resources/template.tpl").execute({foo: "world"}));
	}
	#end
}
