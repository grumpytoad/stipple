package test;

import utest.Assert;

import stipple.Template;
import stipple.AST;

class BlocksTest {
	public function new () { }

	function testArray () {
		Assert.same("goodbye! Goodbye! GOODBYE! cruel world!", new Template ().fromString("{{#goodbyes}}{{text}}! {{/goodbyes}}cruel {{world}}!").execute({ goodbyes: [{text: "goodbye"}, {text: "Goodbye"}, {text: "GOODBYE"}], world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#goodbyes}}{{text}}! {{/goodbyes}}cruel {{world}}!").execute({ goodbyes: [], world: "world" }));
	}

	function testArrayWithDataIndex () {
		Assert.same("0. goodbye! 1. Goodbye! 2. GOODBYE! cruel world!", new Template ().fromString("{{#goodbyes}}{{@index}}. {{text}}! {{/goodbyes}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEmptyBlock () {
		Assert.same("cruel world!", new Template ().fromString("{{#goodbyes}}{{/goodbyes}}cruel {{world}}!").execute({ goodbyes: [{text: "goodbye"}, {text: "Goodbye"}, {text: "GOODBYE"}], world: "world"}));
		Assert.same("cruel world!", new Template ().fromString("{{#goodbyes}}{{/goodbyes}}cruel {{world}}!").execute({ goodbyes: [], world: "world"}));
	}

	function testBlockWithComplexlookup () {
		Assert.same("goodbye cruel Alan! Goodbye cruel Alan! GOODBYE cruel Alan! ", new Template ().fromString("{{#goodbyes}}{{text}} cruel {{../name}}! {{/goodbyes}}").execute({ name: "Alan", goodbyes: [{text: "goodbye"}, {text: "Goodbye"}, {text: "GOODBYE"}]}));
	}

	//function testBlockWithComplexLookupUsingNestedContext () {
		//Assert.raises(function () Template.compile("{{#goodbyes}}{{text}} cruel {{foo/../name}}! {{/goodbyes}}", {}));
	//}

	function testBlockWithDeepNestedComplexLookup () {
		Assert.same("Goodbye cruel OMG!", new Template ().fromString("{{#outer}}Goodbye {{#inner}}cruel {{../../omg}}{{/inner}}{{/outer}}").execute({ omg: "OMG!", outer: [{ inner: [{ text: "goodbye" }] }] }));
	}

	function testInvertedSections () {
		Assert.same("Right On!", new Template ().fromString("{{#goodbyes}}{{this}}{{/goodbyes}}{{^goodbyes}}Right On!{{/goodbyes}}").execute({}));
	}

	function testInvertedSectionsWithFalseValue () {
		Assert.same("Right On!", new Template ().fromString("{{#goodbyes}}{{this}}{{/goodbyes}}{{^goodbyes}}Right On!{{/goodbyes}}").execute({ goodbyes: false }));
	}

	function testInvertedSectionWithEmptySet () {
		Assert.same("Right On!", new Template ().fromString("{{#goodbyes}}{{this}}{{/goodbyes}}{{^goodbyes}}Right On!{{/goodbyes}}").execute({ goodbyes: []}));
	}

	function testBlockInvertedSections () {
		Assert.same("No people", new Template ().fromString("{{#people}}{{name}}{{^}}{{none}}{{/people}}").execute({ none: "No people" }));
	}

	function testBlockInvertedSectionsWithEmptyArrays () {
		Assert.same("No people", new Template ().fromString("{{#people}}{{name}}{{^}}{{none}}{{/people}}").execute({ none: "No people", people: [] }));
	}
}
