package stipple;

import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.ds.Option;

import stipple.Lexer;
import stipple.AST;
using Lambda;

/**
 * An RType is a runtime syntax description. Template context variables are
 * decorated with this type and passed to helpers and partials in scope. You
 * can extract the string value of an RType using Template.extract and
 * Template.extractRType
 */
enum RType {
	Const(str: String);
	Boolean(bool: Bool);
	Func<T, U, V>(args: Array<Dynamic>, cb: RType -> HelperOptionsTyped<T, U, V> -> String);
	Many(arr: Array<RType>);
	Object(ctx: Dynamic);
	Nothing;
}

/**
 * Template execution is normally html-escaped, SafeStrings can be used in
 * helpers to avoid this feature. As such they are subject to CSS-attacks.
 */
class SafeString {
	var string: String;
	public function new (s: String) {
		this.string = s;
	}

	public function toString() {
		return string;
	}
}

/**

 * A HelperOption block is always passed to helper function with information on the
 * template syntax and context at the calling point. 
 *
 * 	- fn: function to execute the inner block, should the template node have
 * 	one.  The first parameter populates the context for the inner block which
 * 	is optionally typed, the second parameter specifies the dynamic @data block
 * 	- inverse: function to execute the 'else' inverse block of a template, it's
 * 	parameters work like the 'fn' function.
 *  - hash: the block of hash parameter keys, with resolved RType enum values.
 *  - ctx: the whole context at the depth of the calling point.

 *  - data: the special @data block can be used to populate differing behaviour
 *  for subsequent executions using the same underlying context.
 *  - name: the original name of the template node.
 *  - params: the set of resolved RType parameter values called with the helper function.
 *  - hashParams: the original set of unresolved hash values, as RType enums values.
 */

typedef HelperOptionsTyped<T, U, V> = {
	var fn: U -> Dynamic -> String;
	var inverse: V -> Dynamic -> String;
	var hash: Dynamic<RType>;
	var ctx: T;
	var data: Dynamic;
	var name: String;
	var params: Array<RType>;
	var hashParams: Array<RType>;
}

typedef HelperOptions = HelperOptionsTyped<Dynamic, Dynamic, Dynamic>
typedef HelperOptionsParam = HelperOptionsTyped<Dynamic, Dynamic, Dynamic>

typedef HelperFuncTyped<T, U, V> = Array<RType> -> HelperOptionsTyped<T, U, V> -> String
typedef HelperFunc = HelperFuncTyped<Dynamic, Dynamic, Dynamic>

// This is an alias type, where the inbuild type checker will use the first
// parameter's resolved type to match block and inverse signatures.
typedef HelperFuncParam = Array<RType> -> HelperOptionsParam -> String


typedef Data = {
	?index: Int,
	?first: Bool,
	?last: Bool,
	?key: String
}

class Template<T> {

	static var initHelpers = Helpers.core();

	// workaround for problems in static initializers.
	var cLocalHelpers = initHelpers;  
	dynamic function localHelpers (): Helpers.Core return cLocalHelpers;
	dynamic function localPartials (): Dynamic<Template<T>> return {};

	var node: Node;

	public function new () {
	}

	function mergeHelpers<T: (Helpers.Core), S, U: (Helpers.Core)>(helpers: S, init: U): T {
		return cast Reflect.fields(helpers).fold(function (key, rest: Dynamic) {
			if (!Reflect.hasField(rest, key)) {
				Reflect.setField(rest, key, Reflect.field(helpers, key));
			}
			return rest;
		}, Reflect.copy(init));
	}

	function mergePartials(partials: Dynamic<Template<T>>, init: Dynamic<Template<T>>) {
		return Reflect.fields(partials).fold(function (key, rest: Dynamic<Template<T>>) {
			if (!Reflect.hasField(rest, key)) {
				Reflect.setField(rest, key, Reflect.field(partials, key));
			}
			return rest;
		}, Reflect.copy(init));
	}

	function getHelper(key: String): Option<HelperFunc> {
		return if (Reflect.hasField(localHelpers(), key)) {
			Some(Reflect.field(localHelpers(), key));
		} else None;
	}

	function getPartial<S>(key: String, ctx: S): Option<Template<T>> {
		return if (Reflect.hasField(localPartials(), key)) {
				Some(Reflect.field(localPartials(), key));
			} else if (Reflect.hasField(ctx, key) && Std.is(Reflect.field(ctx, key), Template)) {
				Some(Reflect.field(ctx, key));
			} else None;
	}


	function accept<S>(ast: Node, ctx: S, data: Dynamic, rest: List<{ast: Node, expr: Dynamic}>): RType {
		return switch (ast) {
			case ProgramNode(statements): 
				switch (statements.array()) {
					case [simple]: 
						accept(simple, ctx, data, rest);
					case stmts: 
						Many(Lambda.map(stmts, function (_: Node): RType {
							return accept(_, ctx, data, rest);
						}).array());
				}
			case MustacheNode(id, params, HashNode(pairs), escaped):

				var idnode = accept(id, ctx, data, rest);
				var hashMap = pairs.fold(function (_, hash: Dynamic<RType>) {
					var e = accept(_.right, ctx, data, rest);
					var s = Std.string(_.left);
					Reflect.setField(hash, s, e);
					return hash;
				}, {});

				var hashParams = pairs.map(function (_) return switch (_.right) {
					case StringLiteralNode(str): Const(str);
					case BooleanNode(bool): Boolean(bool);
					case IdNode(_, _, _, original, _): Object(original);
					case _: Nothing;
				}).array();

				var options: HelperOptionsTyped<S, S, S> = { fn: function (_,?_) return "", inverse: function (_, ?_) return "", ctx: ctx, hash: hashMap, data: (data != null ? data : {}), name: "-", params: [], hashParams: hashParams };

				switch (idnode) {
					case Const(s) if (escaped): Const(StringTools.htmlEscape(s, true));
					case Const(s) if (!escaped): Const(s);
					case Func(args, cb) if (params.empty()): 
						options.name = args[args.length-1].name;
						Func([[args[0]], options], cb);
					case Func(args, cb):
						options.name = args[args.length-1].name;

						options.params = params.array().map(function (_) return switch (_) {
							case StringLiteralNode(str): Const(str);
							case BooleanNode(bool): Boolean(bool);
							case IdNode(_, 0, _, original, _): Object(original);
							case IdNode(parts, depth, isScoped, original, _) if (parts.length > 0 && rest.length - depth >= 0): 
								Reflect.setField(options.ctx, parts.first(), extract([getContextParts([parts.first()].list(), rest.array()[rest.length - depth].expr, data, isScoped, original)])); 
								Object(parts.array().join("."));
							case _: Nothing;
						});

						var a: Array<Dynamic> = [params.map(function (_) {
							return accept(_, ctx, data, rest);
						}).array()];

						a.push(options);

						Func(a, cb);

					case _: idnode;
				}
			case ContentNode(string):
				Const(string);
			case CommentNode(_):
				Nothing;
			case BlockNode(mustache, program, inverse):
				var subctx = accept(mustache, ctx, data, rest);

				var rest = rest.array().list();
				rest.add({ expr: ctx, ast: ast });

				switch (subctx) {
					case Boolean(true): accept(program, subctx, data, rest);
					case Boolean(false): accept(inverse, ctx, data, rest);
					case Func(params, cb): 
						var programCallback = function (_, ?data) { 
							return extractRType(accept(program, _, data, rest)); 
						};
						var inverseCallback = function (_, ?data) { 
							return extractRType(accept(inverse, _, data, rest)); 
						};

						params[params.length - 1].fn = programCallback;
						params[params.length - 1].inverse = inverseCallback;
						params[params.length - 1].data = data;

						Func(params, cb);

					case Many(list):
						var inner = list.mapi(function (i, _) {
							var c: Dynamic = switch (_) {
								case Boolean(b): b;
								case Func(_, cb): cb;
								case Many(a): a;
								case Object(o): o;
								case Const(s): s;
								case Nothing: null;
							}

							return accept(program, c, { index: i, first: (i==0), last: (i == (list.length - 1)) }, rest);
						});

						if (inner.empty()) {
							accept(inverse, ctx, data, rest);
						} else {
							Many(inner.array());
						}
					case Object(n) if (Std.is(n, Int)):
						switch (cast(n, Int)) {
							case 0: accept(inverse, ctx, data, rest);
							case _: accept(program, subctx, data, rest);
						}
					case Object(o):
						accept(program, o, data, rest);
					case Const(s):
						accept(program, s, data, rest);
					case Nothing:
						accept(inverse, ctx, data, rest);
				}

			case NullNode: 
				Nothing;
			case IdNode(parts, 0, isScoped, original, _) if (parts.length > 0):
				getContextParts(parts, ctx, data, isScoped, original);
			case IdNode(parts, depth, isScoped, original, _) if (parts.length > 0 && rest.length - depth >= 0):
				getContextParts(parts, rest.array()[rest.length - depth].expr, data, isScoped, original);
			case IdNode(_, _, _, original, _): 
				if (Std.is(ctx, String) || Std.is(ctx, Int) || Std.is(ctx, Float) || Std.is(ctx, Bool)) {
					Const(Std.string(ctx)); 
				} else if (Std.is(ctx, Array)) {
					Many(cast(ctx, Array<Dynamic>).map(function (_) return toRType(_, ctx, data, original)));
				} else {
					Nothing;
				}
			case IntegerNode(integer): 
				Const(Std.string(integer));
			case DecimalNode(float):
				Const(Std.string(float));
			case BooleanNode(true):
				Boolean(true);
			case BooleanNode(false):
				Boolean(false);
			case DataNode(IdNode(parts, depth, isScoped, original, pos))/* if (parts.length == 1)*/:
				if (isScoped || depth > 0) throw 'Scoped data references are not supported: ${original}';
				var subParts = parts.array();
				var key = subParts.shift();
				if (Reflect.hasField(data, key)) {
					if (subParts.empty()) {
						toRType(Reflect.field(data, key), ctx, data, original);
					} else {
						getContextParts(subParts.list(), Reflect.field(data, key), data, isScoped, original);
					}
				} else {
					Nothing;
				}
			case StringLiteralNode(str):
				Const(str);
			case PartialNode(PartialNameNode(str), ctxnode, HashNode(pairs), _):

				var subctx : Dynamic = switch(accept(ctxnode, ctx, data, rest)) {
					case Many(a): a;
					case Object(o): o;
					case Nothing: ctx;
					case _: {};
				}

				switch (pairs.array()) {
					case [{ left: l, right: IdNode(parts, depth, _, _, _) }] if (rest.length - depth >= 0): 
						Reflect.setField(subctx, l, rest.array()[rest.length - depth].expr);
					case _:
				}

				switch (getPartial(str, ctx)) {
					case Some(template): 

						var mergedHelpers = mergeHelpers(this.localHelpers(), template.localHelpers());
						template.localHelpers = function () return mergedHelpers;

						var mergedPartials = mergePartials(this.localPartials(), template.localPartials());
						template.localPartials = function () return mergedPartials;

						var rest = rest.array().list();
						rest.add({ expr: ctx, ast: ast });

						if (template.node != null) {
							Const(extractRType(template.accept(template.node, subctx, data, rest)));
						} else throw "Partial requires a compiled template through (fromString)";

					case None: throw 'Runtime template cannot find partial: "${str}"';
				}

			case _: throw 'Implementation missing for template compile-time AST node: ${ast}';

		}
	}

	function toRType(val: Dynamic, ctx: Dynamic, data: Dynamic, name: String) return switch (val) {
		case b if (Std.is(b, Bool)): Boolean(cast(b, Bool));
		case f if (Reflect.isFunction(f)): 
			Func([toRType(ctx, null, data, name), { fn: function (_, ?opts) return "", inverse: function (_, ?opts) return "", hash: new Map<String, RType>(), ctx: ctx, data: data, name: name}], f);
		case a if (Std.is(a, Array)): Many(cast(a, Array<Dynamic>).map(function (_) return toRType(_, ctx, data, name)));
		case n if (n==null): Nothing;
		case s if (Std.is(s, String)): Const(Std.string(s));
		case e if (Std.is(e, RType)): e;
		#if cpp
		//case n if (Std.is(n, Int)):
		//	switch (cast(val, Int)) {
		//		case 0: Boolean(false);
		//		case _: Boolean(true);
		//	}
		#end
		case v: Object(v);
	}

	static var helperMissingKey = "helperMissing";
	static var literalReg = ~/^\[?([^\]]+)\]?/; 
	function getContextParts<V>(args: List<String>, ctx: Dynamic, data: Dynamic, isScoped: Bool, original: String): RType {
		return toRType(switch (getHelper(args.first())) {
			case Some(helper) if (!isScoped): helper;
			case _: 
				args.fold(function (part, rest: Dynamic) return {
				var klass = Type.getClass(rest);
				var fields = (Std.is(rest, Bool) ? [] : (klass != null ? Type.getInstanceFields(klass) : Reflect.fields(rest)));
				switch (fields.filter(function (_) {
					// literal is Str([...])
					literalReg.match(part); 
					return _ == literalReg.matched(1);
					// HACK to read JSON-notated fields (DEPRECATED)
					//return StringTools.replace(_, "@$__hx__", "") == literalReg.matched(1); 
				})) {
					case [key]: Reflect.field(rest, key);
					case v: 
						var ret: Dynamic = switch (getHelper(helperMissingKey)) {
							case Some(fallback): fallback;
							case None: false;
						}
						return ret;
				}
			}, ctx);
		}, ctx, data, original);
	}
	public function setNode(node: Node) {
		this.node = node;
		return this;
	}

	static function getOrElse <A>(_: Option<A>, o:A) return switch (_) {
		case Some(v): v;
		case None: o;
	}

	#if !stipple_noprecompile
	macro public function fromString(self: Expr, tpl: ExprOf<String>): Expr {
		return switch (tpl.expr) {
			case EConst(CString(templateString)):

				var inf = Context.getPosInfos(tpl.pos);

				// need to offset the string position by two (?)
			  var shiftPos = Context.makePosition({min: inf.min + 2, max: inf.max, file: inf.file});
				var ast = if (templateString.length > 0) {
					var p = new HpsParser(byte.ByteData.ofString(templateString)).parse();
					AST.parse(p, shiftPos);
				} else {
					NullNode;
				}

				var nodeExpr = ASTUtil.toExpr(ast);

				return macro {
					$self.setNode($nodeExpr);
				}
			case v: Context.error('Template.fromString requires a constant string: ${v}', Context.currentPos());
		}
	}
	#else

	public function fromString(tpl: String) {
			var ast = if (tpl.length > 0) {
				var p = new HpsParser(byte.ByteData.ofString(tpl)).parse();
				AST.parse(p);
			} else {
				NullNode;
			}

			return this.setNode(ast);
	}
	#end

	#if sys
		#if !stipple_noprecompile
	macro public function fromFile(self: Expr, file: ExprOf<String>): Expr {
		return switch (file.expr) {
			case EConst(CString(fileName)) if (sys.FileSystem.exists(fileName)):
				
				var templateString = try {
					sys.io.File.getContent(fileName);
				} catch (e: Dynamic) {
					Context.error('File not readable: ${fileName}', Context.currentPos());
				}
				var shiftPos = Context.makePosition({ min: 1, max: 1, file: sys.FileSystem.fullPath(fileName) });

				var ast = if (templateString.length > 0) {
					var p = new HpsParser(byte.ByteData.ofString(templateString)).parse();
					AST.parse(p, shiftPos);
				} else {
					NullNode;
				}

				var nodeExpr = ASTUtil.toExpr(ast);

				return macro {
					$self.setNode($nodeExpr);
				}
			case EConst(CString(fileName)):
				Context.error('File not found: ${fileName}', Context.currentPos());
			case v: Context.error('Template.fromFile requires a constant string: ${v}', Context.currentPos());
		}
	}
		#else

	public function fromFile(fileName: String) {
		if (sys.FileSystem.exists(fileName)) {
				var templateString = try {
					sys.io.File.getContent(fileName);
				} catch (e: Dynamic) {
					throw 'File not readable (permissions?): ${fileName}';
				}

				var ast = {
					var p = new HpsParser(byte.ByteData.ofString(templateString)).parse();
					AST.parse(p);
				}

				return this.setNode(ast);
		} else {
			throw 'File not found: ${fileName}';
		}
	}
		#end
	#end

	public function execute (context: T) {
		return if (this.node != null) {
			extractRType(accept(this.node, context, {}, new List()));
		} else throw "Template.execute requires a compiled template through (fromString)";
	}

	public static function extractRType(n: RType): String {
		return switch (n) {
			case Const(s): s;
			case Many(a): 
				a.fold(function (_: RType, rest: StringBuf) {
					rest.add(extractRType(_));
					return rest;
				}, new StringBuf()).toString();
			case Nothing: "";
			case Func(args, cb): Std.string(Reflect.callMethod(null, cb, args));
			case Object(o) if (Std.is(o, Int)): Std.string(o);
			case Object(o) if (Std.is(o, Float)): Std.string(o);
			case Object(o) if (Std.is(o, SafeString)): o.toString();
			case Object(o): "";
			case Boolean(_): "";
		}
	}

	public static function extract(arr: Array<RType>, ?spacing: String): String {
		return if (spacing != null) {
			Lambda.map(arr, function (_) return Template.extractRType(_)).join(spacing);
		} else {
			Lambda.fold(arr, function (_, rest: StringBuf) { 
				rest.add(Template.extractRType(_));
				return rest;
			}, new StringBuf()).toString();
		}
	}
}

