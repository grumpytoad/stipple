package stipple;

using StringTools;
using Lambda;

import haxe.macro.Expr;
import haxe.macro.Context;

using Std;

enum ASTExprDef {
	Complex(expr: ASTExpr, pos: Int);
	List(expr: Array<ASTExprDef>);
	Str(expr: String);
	None;
}

typedef ASTExpr = {
	var name : String;
	var value : ASTExprDef;
}

enum ERule {
	STRING(string: String);
	REGEXP(regExp: EReg, ?mPos: Int);
	FUNCTION(func: Void -> ERule, name: String, ?complex: Bool);
	SEQUENCE(rules: Array<ERule>);
	OR(rules: Array<ERule>);
	MAYBE(rule: ERule);
	MANY(rule: ERule, ?seperator: ERule);
	TOKEN(rule: ERule);
	RAISE(func: { lineNumber : Int, lineOffset : Int, lineData : String } -> ?Position -> Void);
	SKIPWHITE(rule: ERule);
	NEGLOOKAHEAD(string: String);
	REGEXPPOSLOOKBEHIND(regExp: EReg, strings: Array<String>, ?mPos: Int);
	REGEXPNEGLOOKBEHIND(regExp: EReg, strings: Array<String>, ?mPos: Int);
}

enum MatchRes {
	None;
	Some(pos: Int, value: ASTExprDef);
}

typedef PegParserState = {
	var stringsAreTokens : Bool;
	var text : String;
	var maxPos : Int;
	var maxRule : Array<String>;
	var debug : Bool;
	var verbose : Bool;
	var cache : Map<String, MatchRes>;
	var startPos: Position;
}

class PegParser {

	static public function parse(grammar: ERule, content: String, stringsAreTokens = false, debug = false, verbose = true, pos: Position = null) {
		var state = { text : content, stringsAreTokens : stringsAreTokens, maxPos: 0, maxRule: [], debug: debug, verbose: verbose, cache: new Map(), startPos: pos };
		return switch(PegParser.match(grammar, state, 0)) {
			case Some(pos, value): 
				if (skipWhiteSpace(state, pos) == content.length) value; else error(state, ASTExprDef.None);
			case None: error(state, ASTExprDef.None);
		};
	}

	static function error(state: PegParserState, res: ASTExprDef) {
		var error = analyze(state.maxPos, state.text);
		if (state.maxRule.length > 0) {
			var expected = state.maxRule.join("' or '");
			var found = state.text.substr(state.maxPos, 1);
			if (state.verbose) {
				trace('Unexpected error at line ${error.lineNumber} offset ${error.lineOffset}: expected "${expected}", but found "${found}" --${error.lineData}');
			}
		} else {
			if (state.verbose) {
				trace('Unexpected error at line ${error.lineNumber} offset ${error.lineOffset}: ${error.lineData}');
			}
		}
		return res;
	}

	static function analyze(globalOffset: Int, text: String) {
		var i = 0, lineNumber = 1, lineOffset = 0, lineData = [];
		while (i < globalOffset && i < text.length) {
			if (text.substr(i, 1) == "\n") {
				lineNumber += 1;
				lineOffset = 0;
				lineData = [];
			} else {
				lineData.push(text.substr(i, 1));
				lineOffset +=1;
			}
			i += 1;
		}
		while (i < text.length && text.substr(i, 1) != "\n") {
			lineData.push(text.substr(i, 1));
			i += 1;
		}

		return { lineNumber : lineNumber, lineOffset : lineOffset, lineData : lineData.join('')};
	}

	public static function dump<T>(ast: ASTExprDef): T {
		return switch (ast) {
			case Complex(e, p): cast { name : e.name, value : dump(e.value), pos: p };
			case List(a): cast Lambda.map(a, function (e) return dump(e)).array();
			case Str(s): cast s;
			case None: null;
		}
	}

	static var white = [
		REGEXP(~/^\s+/)
	];

	static public function skipWhiteSpace(state: PegParserState, pos: Int) {

		while (true) {
			var nextPos = pos;
			for (regex in white) switch (matchNext(regex, state, nextPos)) {
				case Some(pos, _): nextPos = pos;
				default: 
			}
			if (nextPos == pos) {
				pos = nextPos;
				break;
			} else pos = nextPos;
		}
		return pos;
	}


	static public function match (rule: ERule, state: PegParserState, pos: Int, skipWhite: Bool = false) {
		if (skipWhite) {
			pos = skipWhiteSpace(state, pos);
		}

		// Track position for error debugging

		if (pos > state.maxPos) {
			state.maxPos = pos;
			switch (rule) {
				case STRING(str): state.maxRule = [str];
				default: state.maxRule = [];
			}
		} else if (pos == state.maxPos) {
			switch (rule) {
				case STRING(str): state.maxRule.push(str);
				default: 
			}
		}

		if (state.debug) trace('Try: pos=${pos} char=${state.text.substr(pos, 1)} rule=${rule}');

		var res = switch (rule) {
			case FUNCTION(_, name, _): 
				var comparable = name + Std.string(pos);
				var cacheHit = state.cache.get(comparable);
				if (cacheHit != null) {
					cacheHit;
				} else {
					var r = matchNext(rule, state, pos, skipWhite);
					state.cache.set(comparable, r);
					r;
				}
			case _: matchNext(rule, state, pos, skipWhite);
		}

		if (state.debug) {
			switch (res) {
				case Some(_): trace('Match: pos=${pos} char=${state.text.substr(pos, 1)} rule=${rule}');
				case None: trace('Fail: pos=${pos} char=${state.text.substr(pos, 1)} rule=${rule}');
			}
		}

		return res;
	}

	static public var timings: Map<String, Float> = new Map();
	inline static function bench(name: String, func: Void -> MatchRes) {
		var time = haxe.Timer.stamp();
		var res = func();
		if (timings.get(name) != null) {
			timings.set(name, timings.get(name) + (haxe.Timer.stamp() - time));
		} else {
			timings.set(name, (haxe.Timer.stamp() - time));
		}
		return res;
	}

	static public function matchNext (rule: ERule, state: PegParserState, pos: Int, skipWhite: Bool = false) 
		return switch (rule) {
			case STRING(string): 
				if (state.text.substr(pos).startsWith(string)) {
					var nextPos = pos + string.length;
					if (state.stringsAreTokens) Some(nextPos, None)
						else Some(nextPos, Str(string));
				} else None;

			case NEGLOOKAHEAD(string): 
				if (!state.text.substr(pos).startsWith(string)) {
					Some(pos, None);
				} else None;

			case REGEXP(regExp, mPos):
				mPos = mPos != null ? mPos : 0;
				if (regExp.match(state.text.substr(pos))) {
					var matchStr = regExp.matched(0);
					Some(pos + matchStr.length + mPos, Str(matchStr.substr(0, matchStr.length + mPos)));
				} else { None; }

			case REGEXPPOSLOOKBEHIND(regExp, strings, mPos):
				mPos = mPos != null ? mPos : 0;
				if (regExp.match(state.text.substr(pos))) {
					var matchStr = regExp.matched(0);
					if (!strings.filter(function (_) return matchStr.endsWith(_)).empty()) {
						Some(pos + matchStr.length + mPos, Str(matchStr.substr(0, matchStr.length + mPos)));
					} else {
						None;
					}
				} else { None; }

			case REGEXPNEGLOOKBEHIND(regExp, strings, mPos):
				mPos = mPos != null ? mPos : 0;
				if (regExp.match(state.text.substr(pos))) {
					var matchStr = regExp.matched(0);
					if (strings.filter(function (_) return matchStr.endsWith(_)).empty()) {
						Some(pos + matchStr.length + mPos, Str(matchStr.substr(0, matchStr.length + mPos)));
					} else {
						switch (match(REGEXPNEGLOOKBEHIND(regExp, strings, mPos), state, pos + matchStr.length + mPos, skipWhite)) {
							case Some(nPos, Str(nStr)): Some(nPos, Str(matchStr.substr(0, matchStr.length + mPos) + nStr));
							case _: None;
						}
					}
				} else { None; }

			case FUNCTION(func, name, complex):
				complex = complex == null ? true : complex;
				switch(match(func(), state, pos, skipWhite)) {
					case Some(p, value): 
						if (complex) 
							Some(p, Complex({ name : name, value : value }, pos));
						else
							Some(p, value);
					case None: None;
				}

			case SEQUENCE(rules):
				rules.fold(function (rule, res) 
					return switch (res) {
						case Some(restpos, restvalue): 
							switch (restvalue) {
								case List(list):
									switch (match(rule, state, restpos, skipWhite)) {
										case Some(pos, value):
											switch (value) {
												case List(a): 
													restvalue = List(list.concat(a));
												case None:
												default: 
													list.push(value);
													restvalue = List(list);
											}
											restpos = pos;
											Some(restpos, restvalue);
										case None: None;
									}
								default: None;
							}
						case None: None;
					}, Some(pos, List([])));

			case OR(rules):
				rules.fold(function (rule, res)
					return switch (res) {
						case Some(restpos, restvalue): Some(restpos, restvalue);
						case None: 
							switch (match(rule, state, pos, skipWhite)) {
								case Some(pos, value): Some(pos, value);
								case None: None;
							}
					}, None);

			case MAYBE(rule):
				switch (match(rule, state, pos, skipWhite)) {
					case Some(pos, value): Some(pos, value);
					case None: Some(pos, None);
				}
			case MANY(rule, seperator):
				var res = [], count = 0, p = 0;
				while (true) {
					if (count > 0 && seperator != null) {
						switch (match(seperator, state, pos, skipWhite)) {
							case Some(p, value): 
								pos = p;
								switch (value) {
									case None:
									default: res.push(value);
								}
							case None: break;
						}
					}
					switch (match(rule, state, pos, skipWhite)) {
						case Some(p, value):
							count += 1;
							pos = p;
							res.push(value);
						case None: break;
					}
				}
				if (count > 0) Some(pos, List(res));
				else None;
			case TOKEN(rule):
				switch (match(rule, state, pos, skipWhite)) {
					case Some(pos, _): Some(pos, None);
					case None: None;
				}
			case SKIPWHITE(rule):
				switch (match(rule, state, pos, true)) {
					case Some(pos, value): Some(pos, value);
					case None: None;
				}
			case RAISE(func):
				var error = analyze(state.maxPos, state.text);
				#if macro
				func(error, AST.offsetPosition(state.startPos, pos)); 
				#else
				func(error); 
				#end

				None;
		}
}

