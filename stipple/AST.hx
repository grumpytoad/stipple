package stipple;

using Lambda;
using StringTools;
import haxe.macro.Context;
import haxe.macro.Expr;

enum ASTExprDef {
	Complex(expr: ASTExpr, pos: Int);
	List(expr: Array<ASTExprDef>);
	Str(expr: String);
	None;
}

typedef ASTExpr = {
	var name : String;
	var value : ASTExprDef;
}

class AST {

	static public function parse(_: ASTExprDef, ?pos: Position): Node return switch (_) {
		case List(a): createProgramNode(a, pos);
		default: NullNode;
	}

	static function parseListAST (a: Array<ASTExprDef>, rest: List<Node>, pos: Position) {
		switch (a[0]) {
			case Complex({name: "OPEN", value: _}, offset): rest.add(createMustacheNode(a, offsetPosition(pos, offset))); return rest;
			case Complex({name: "OPEN_PARTIAL", value: _}, offset): rest.add(createPartialNode(a, offsetPosition(pos, offset))); return rest;
			case Complex({name: "OPEN_BLOCK", value: _}, offset): rest.add(createBlockNode(a, offsetPosition(pos, offset))); return rest;
			case Complex({name: "OPEN_INVERSE", value: _}, offset): rest.add(createBlockNode(a, offsetPosition(pos, offset))); return rest;
			case Complex({name: "OPEN_UNESCAPED", value: _}, offset): rest.add(createMustacheNode(a, offsetPosition(pos, offset))); return rest;
			default:
		}
		Lambda.map(a, function (_) {
			switch (_) {
				case Complex({name: "INNERCONTENT", value: v}, _): rest.add(createContentNode(v)); 
				case Complex({name: "OPEN_ENDBLOCK", value: v}, offset): createError('Close block found without a matching open block', offsetPosition(pos, offset));
				case List(v): parseListAST(v, rest, pos);
				case v: createError('Unhandled template node "${v}" while parsing AST', pos);
			}
		});
		return rest;
	}

	static function createError<T>(message: String, pos: Position): T return {
		#if macro 
		Context.error(message, pos != null ? pos : Context.currentPos());
		#else
		throw message;
		#end
	}

	public static function offsetPosition(pos: Position, offset: Int): Position {
		#if (macro && !stipple_noprecompile)
			var inf = Context.getPosInfos(pos);
			return Context.makePosition({ min: inf.min + offset, max: inf.max, file: inf.file });
		#else
			// redundant
			return pos;
		#end
	}
		
	static function createProgramNode(a: Array<ASTExprDef>, pos: Position) {

		var statements = new List();

		Lambda.map(a, function (_) switch (_) {
			case Complex({name: "INNERCONTENT", value: v}, _): statements.add(createContentNode(v));
			case Complex({name: "CONTENT", value: v}, _): statements.add(createContentNode(v));
			case Complex({name: "COMMENT", value: v}, _): statements.add(createCommentNode(v));
			case List(a):
				parseListAST(a, statements, pos);
			case v: createError('Unparseable template node "${v}"', pos);
		});

		return ProgramNode(statements);
	}

	static function separatePathComponents(a: Array<ASTExprDef>) {
		return a.fold(function (e, rest: List<List<ASTExprDef>>) return switch (e) {
			case List([Complex({name: "SEP", value: _}, _), Complex({name: "ID", value: _}, _)]):
				rest.last().add(e); rest;
			case Complex({name: "ID", value: v}, _):
				var n = new List(); n.add(v);
				rest.add(n); rest;
			default: rest;
		}, new List());
	}

	static function extractString(astExprDef: ASTExprDef) {
		var replaceEscapedCurly = function (s: String) {
			var n = new StringBuf(), idx = -1, prefix = false;
			while (true) {
				idx = s.indexOf("\\{{");
				if (idx >= 0) {
					prefix = s.substring(idx - 2, 2) != "\\\\";
					if (prefix) {
						n.add(s.substring(0, idx));
						n.add("{{");
						s = s.substring(idx+3);
					} else {
						n.add(s.substring(0, idx+3));
						s = s.substring(idx+3);
					}
				} else {
					n.add(s);
					break;
				}
			}
			return n.toString();
		}

		return switch (astExprDef) {

			case Str(s): 
				replaceEscapedCurly(s);
			case List(a): a.map(extractString).join("");
			default: "";
		}
	}

	static function createContentNode(astExprDef: ASTExprDef) return ContentNode(extractString(astExprDef));

	static function createCommentNode(_: ASTExprDef) return switch (_) {
		case Str(v): CommentNode(v);
		default: NullNode;
	}

	static function createDataOrPath(e: ASTExprDef, pos: Position): Array<ASTExprDef> -> Node return switch (e) {
		case Complex({name: "DATA", value: _}, offset):
			function (_) { return createDataNode(_, offsetPosition(pos, offset)); }
		case Complex({name: "ID", value: _}, offset):
			function (_) { return createIdNode(_, offsetPosition(pos, offset)); }
		default: createError('Mustache expected a DATA token (@) or a valid path, got: ${e}', pos);
	}

	static function createMustacheNode(a: Array<ASTExprDef>, pos: Position) 
		return MustacheNode(

			createDataOrPath(a[1], pos)(separatePathComponents(a).first().array()),

			Lambda.map(a.filter(function (e) return switch (e) {
				case List([Complex({name: "SEP", value: _}, _), _]): false;
				case List(l): if (l.length > 2) {
					switch (l[1]) {
						case Complex({name: "EQUALS", value: _}, _): false;
						default: true;
					}
				} else true;
				case Complex({name: "STRINGLITERAL", value: _}, _): true;
				case Complex({name: "INTEGER", value: _}, _): true;
				case Complex({name: "DECIMAL", value: _}, _): true;
				case Complex({name: "BOOLEAN", value: _}, _): true;
				case Complex({name: "ID", value: _}, pos) if (e != a[1]): 
					switch (a[1]) {
						case Complex({name: "DATA", value: _}, _): false;
						default: true;
					}
				default: false;
			}), function (_) return switch (_) {
				case List(l): cast createDataOrPath(l[0], pos)(separatePathComponents(l).first().array());
				case Complex({name: "STRINGLITERAL", value: v}, _): cast createStringLiteralNode(v);
				case Complex({name: "INTEGER", value: v}, _): cast createIntegerNode(v);
				case Complex({name: "DECIMAL", value: v}, _): cast createDecimalNode(v);
				case Complex({name: "BOOLEAN", value: v}, _): cast createBooleanNode(v);
				case Complex({name: "ID", value: v}, offset): 
					cast createIdNode([v], offsetPosition(pos, offset));
				default: null;
			}).list(),

			createHashNode(a.filter(function (_) return switch (_) {
				case List(l) if (l.length > 2): 
					switch (l[1]) {
						case Complex({name: "EQUALS", value: _}, _): true;
						default: false;
					}
				default: false;
			}).fold(function (_, rest: List<{ left: String, right: Node }>) return switch (_) {
				case List(l):
					var n: { left: String, right: Node } = { left: null, right: null };
					switch(l[0]) {
						case Complex({name: "ID", value: Str(v)}, _): n.left = v;
						default:
					}
					switch(l[2]) {
						case Complex({name: "STRINGLITERAL", value: v}, _):
							n.right = cast createStringLiteralNode(v);
						case Complex({name: "INTEGER", value: v}, _):
							n.right = cast createIntegerNode(v);
						case Complex({name: "DECIMAL", value: v}, _):
							n.right = cast createDecimalNode(v);
						case Complex({name: "BOOLEAN", value: v}, _):
							n.right = cast createBooleanNode(v);
						case Complex({name: "ID", value: v}, offset): 
							n.right = cast createIdNode([v].concat(l.slice(2)), offsetPosition(pos, offset));
						case Complex({name: "DATA", value: _}, offset):
							n.right = cast createDataNode(separatePathComponents(l.slice(2)).first().array(), offsetPosition(pos, offset));
						default: 
					}
					if (n.left != null && n.right != null) rest.add(n);
					rest;
				default: null;
			}, new List())),
				
			switch (a[0]) {
				case Complex({name: "OPEN_UNESCAPED", value: _}, _): false;
				case Complex({name: "OPEN", value: Str("{{&")}, _): false;
				default: true;
			});

	static function createIdNode(a: Array<ASTExprDef>, pos: Position) {
		return IdNode(
			a.fold(function (_, rest: List<String>) return switch (_) {
				case Str(part): 
					if (part == ".." || part == "." || part == "this") {
					} else {
						if (part.charCodeAt(0) == 91 && part.charCodeAt(part.length - 1) == 93) {
							// escaped with '[' and ']'
							rest.add(part.substring(1, part.length - 1));
						} else {
							rest.add(part); 
						}
					}
					rest;
				case List([Complex({name: "SEP", value: _}, _), Complex({name: "ID", value: Str(part)}, offset)]):
					if ((!rest.empty() && part == "..") || part == "." || part == "this") {
						var original = a.fold(function (_, rest: String) return switch (_) {
							case Str(part): rest + part;
							case List([Complex({name: "SEP", value: Str(sep)}, _), Complex({name: "ID", value: Str(part)}, offset)]): rest + sep + part;
							default: rest;
						}, "");

						createError('Segment (${part}) should exist at start of a path: "${original}"', offsetPosition(pos, offset));
					} else {
						if (part != "..")
							rest.add(part); 
					}
					rest;
				default: rest;
			}, new List()),
			a.fold(function (_, rest: Int) {
				return switch (_) {
					case Str(".."): rest + 1;
					case List([Complex({name: "SEP", value: _}, _), Complex({name: "ID", value: Str("..")}, _)]): rest + 1;
					case v: rest;
				}
			}, 0),
			switch (a[0]) {
				case Str(".."): false;
				case Str("."): true;
				case Str("this"): true;
				default: false;
			},
			a.fold(function (_, rest: String) return switch (_) {
				case Str(part): rest + part;
				case List([Complex({name: "SEP", value: Str(sep)}, p), Complex({name: "ID", value: Str(part)}, _)]): rest + sep + part;
				default: rest;
			}, ""), 
			pos);
		}

	static function createIntegerNode(_: ASTExprDef) return switch (_) {
		case Str(v): IntegerNode(Std.parseInt(v));
		default: NullNode;
	}

	static function createDecimalNode(_: ASTExprDef) return switch (_) {
		case Str(v): DecimalNode(Std.parseFloat(v));
		default: NullNode;
	}

	static function createDataNode(a: Array<ASTExprDef>, pos: Position) 
		return DataNode(createIdNode(a, pos));

	static function createStringLiteralNode(_: ASTExprDef) return switch (_) {
		case Str(v): StringLiteralNode(v.substr(1,v.length-2).replace('\\"', '"').replace("\\'", "'"));
		default: NullNode;
	}

	static function createBooleanNode(_: ASTExprDef) return switch (_) {
		case Str("true"): BooleanNode(true);
		case Str("false"): BooleanNode(false);
		default: NullNode;
	}

	static function createHashNode(a: List<{ left: String, right: Node}>) 
		return HashNode(a);

	static function createPartialNode(a: Array<ASTExprDef>, pos: Position) {
	 if (a.exists(function (_) return switch (_) {
			case Complex({name: "EQUALS", value: _}, _): true;
			case _: false;
		})) {
			var components = separatePathComponents(a.slice(0, a.length - 4));
			var hash = new List();
			hash.add({ left: switch (a[a.length - 4]) {
				case Complex({name: "ID", value: Str(v)}, _): v;
				default: "-";
			}, right: switch (a[a.length - 2]) {
				case Complex({name: "STRINGLITERAL", value: v}, _): createStringLiteralNode(v);
				case Complex({name: "INTEGER", value: v}, _): createIntegerNode(v);
				case Complex({name: "DECIMAL", value: v}, _): createDecimalNode(v);
				case Complex({name: "BOOLEAN", value: v}, _): createBooleanNode(v);
				case Complex({name: "ID", value: v}, offset): createIdNode([v], offsetPosition(pos, offset));
				default: NullNode;
			}});

			return PartialNode(
				createPartialNameNode(a, pos),
				if (components.count() > 1) createIdNode(components.last().array(), pos) else NullNode, HashNode(hash), pos);

		} else {
			var components = separatePathComponents(a);
			return PartialNode(createPartialNameNode(a, pos),
				if (components.count() > 1) createIdNode(components.last().array(), pos) else NullNode, HashNode(new List()), pos);
		}
	}

	static function createPartialNameNode(a: Array<ASTExprDef>, pos: Position) 
		return PartialNameNode(switch (a[1]) {
			case Complex({name: "ID", value: _}, offset): 
				switch (createIdNode(separatePathComponents(a).first().array(), offsetPosition(pos, offset))) {
					case IdNode(_, _, _, original, _): 
						if (original.charCodeAt(0) == 91 && original.charCodeAt(original.length - 1) == 93) {
							// escaped with '[' and ']'
							original.substring(1, original.length - 1);
						} else {
							original;
						}
					default: ""; 
				}
			case Complex({name: "INTEGER", value: v}, _): 
				switch (createIntegerNode(v)) {
					case IntegerNode(integer): Std.string(integer);
					default: "";
				}
			case Complex({name: "DECIMAL", value: v}, _): 
				switch (createDecimalNode(v)) {
					case DecimalNode(float): Std.string(float);
					default: "";
				}
			case Complex({name: "STRINGLITERAL", value: v}, _): 
				switch (createStringLiteralNode(v)) {
					case StringLiteralNode(string): string;
					default: "";
				}
			default: "";
		});

	static function createBlockNode(a: Array<ASTExprDef>, pos: Position) {

		var block = a.fold(function (e, rest:List<List<ASTExprDef>>) return switch (e) {
			case Complex({name: "OPEN_ENDBLOCK", value: _}, _): rest.add(new List()); rest.last().add(e); rest;
			case Complex({name: "OPEN_BLOCK", value: _}, _): rest.add(new List()); rest.last().add(e); rest;
			case Complex({name: "CLOSE", value: _}, _): rest.last().add(e); if (e != a[a.length-1]) rest.add(new List()); rest;
			case Complex({name: "OPEN_INVERSE", value: _}, _): rest.add(new List()); rest.last().add(e); rest;
			case _: rest.last().add(e); rest;
		}, new List());

		var extractId = function (a: Array<ASTExprDef>) {
			return Lambda.map(a.filter(function (_) return switch (_) {
				case Complex({name: "ID", value: _}, _): true;
				default: false;
			}), function (_) return switch (_) {
				case Complex({name: "ID", value: Str(v)}, _): v;
				default: "";
			}).array().shift();
		}

		switch ([extractId(block.first().array()), extractId(block.last().array())]) {
			case [a, b] if (Std.string(a) != Std.string(b)): createError('Start of block ${a} does not match end of block ${b}', pos);
			default:
		}

		var mustache: Node = NullNode, program: Node = NullNode, inverse: Node = NullNode;

		var extractFromStatements = function (a: Array<ASTExprDef>) return {
			a.fold(function (_, rest: List<List<ASTExprDef>>) {
				return switch (_) {
					case List([Complex({name: "OPEN_INVERSE", value: _}, _), Complex({name: "CLOSE", value: _}, _)]): 
						rest.add(new List()); rest;
					case e: rest.last().add(e); rest;
				}
			}, { var s = new List(); s.add(new List()); s; });
		}

		switch (block.first().first()) {
			case Complex({name: "OPEN_INVERSE", value: _}, offset):
				var inv = block.pop().array();
				if (separatePathComponents(inv).isEmpty()) createError("Cannot determine nodename from unqualified inverse node '{{^}}'", offsetPosition(pos, offset));
				mustache = createMustacheNode(inv, pos);
			default:
				mustache = createMustacheNode(block.pop().array(), pos);
				if (block.empty()) {
					switch (mustache) {
						case MustacheNode(IdNode(_, _, _, original, _), _): 
							createError('Open block without an end block: "${original}"', pos);
						case _:
							createError("Open block without an end block", pos);
					}
				}
				switch (block.pop().array()) {
					case [Complex({name: "STATEMENTS", value: List(a)}, _)]: 
						switch(extractFromStatements(a).array()) {
							case [p]: 
								program = createProgramNode(p.array(), pos);
							case [p, i] if (p.empty() && !i.empty()):
								inverse = createProgramNode(i.array(), pos);
							case [p, i] if (!p.empty() && !i.empty()):
								program = createProgramNode(p.array(), pos);
								inverse = createProgramNode(i.array(), pos);
							case [p, i] if (!p.empty() && i.empty()):
								program = createProgramNode(p.array(), pos);
							case _:
						}
					case [Complex({name: "STATEMENT", value: v}, _)]: 
						program = createProgramNode([v], pos);

					default: 
				}
				block.pop();
		}

		if (block.first() != null)
			inverse = switch (block.pop().array()) {
				case [Complex({name: "STATEMENTS", value: v}, _)]: createProgramNode([v], pos);
				case [Complex({name: "STATEMENT", value: v}, _)]: createProgramNode([v], pos);
				default: NullNode;
			}

		return BlockNode(mustache, program, inverse);
	}

	public static function dump<T>(ast: ASTExprDef): T {
		return switch (ast) {
			case Complex(e, p): cast { name : e.name, value : dump(e.value), pos: p };
			case List(a): cast Lambda.map(a, function (e) return dump(e)).array();
			case Str(s): cast s;
			case None: null;
		}
	}
}

class ConsoleWriter {
	static public function dump(node: Node) {
		return switch (node) {
			case ProgramNode(statements): statements.map(function (_) return dump(_)).join(""); 
			case MustacheNode(id, params, hash, _): '{{ ${dump(id)} [${params.map(function (_) return dump(_)).join(", ")}] ${dump(hash)}}}';
			case IdNode(parts, _, _, _, _) if (parts.length > 1): "PATH:" + parts.join("/"); 
			case IdNode(parts, _, _, _, _): "ID:" + parts.join("/");
			case DataNode(id): '@${dump(id)}';
			case StringLiteralNode(string): '"${string}"';
			case IntegerNode(integer): 'INTEGER{${integer}}';
			case DecimalNode(decimal): 'DECIMAL[${decimal}}';
			case BooleanNode(boolean): 'BOOLEAN{${boolean}}';
			case HashNode(pairs) if (pairs.count() == 0): "";
			case HashNode(pairs): "HASH{" + pairs.map(function (_) return '${_.left}=${dump(_.right)}').join(", ") + "} ";
			case ContentNode(string): 'CONTENT[ \'${string}\' ]';
			case PartialNode(partialName, context, _): 
				'{{> ${dump(partialName)} ' + (switch (context) {
					case NullNode: "";
					case context: dump(context) + " ";
				}) + "}}";
			case CommentNode(comment): '{{! \'${comment}\' }}';
			case PartialNameNode(name): 'PARTIAL:${name}';
			case BlockNode(mustache, program, inverse): 
				'BLOCK: ${dump(mustache)}' + (switch (program) {
					case NullNode: "";
					case _: " PROGRAM: " + dump(program);
				}) + (switch (inverse) {
					case NullNode: "";
					case _: " {{^}} " + dump(inverse);
				});
			case NullNode: "";
		}
	}
}

enum Node {
	ProgramNode(statements: List<Node>);
	MustacheNode(id: Node, params: List<Node>, hash: Node, escaped: Bool);
	IdNode(parts: List<String>, depth: Int, isScoped: Bool, original: String, pos: Position);
	DataNode(id: Node);
	StringLiteralNode(string: String);
	IntegerNode(integer: Int);
	DecimalNode(decimal: Float);
	BooleanNode(boolean: Bool);
	HashNode(pairs: List<{left: String, right: Node}>);
	ContentNode(string: String);
	PartialNode(partialName: Node, context: Node, hash: Node, pos: Position); 
	CommentNode(string: String);
	PartialNameNode(name: String);
	BlockNode(mustache: Node, program: Node, inverse: Node);
	NullNode;
}

