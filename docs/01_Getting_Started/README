## Getting started

Stipple syntax looks and works a lot like [Handlebars](http://handlebarsjs.com). Template precompilation is baked into the compile stage of the haxe compiler. Template strings can be stored in string variables or separate files, they should not be stored in website tags.

### Dependencies

In order to start coding with stipple you need the _development_ branch of the [haxe compiler](https://github.com/HaxeFoundation/haxe/tree/development) installed. 

Stipple also depends on a _development_ version of the [hxparse](https://github.com/Simn/hxparse/tree/development) library, which is provided through git submodules. 

### Template syntax

The base class `stipple.Template` acts as a gateway for parsing and contextualising template strings. 

```
import stipple.Template;
```

You can use the `fromString` and `fromFile` static methods on the Template class to compile a template string:

```
var tpl = new Template ().fromString(
  "Hello {{language}}"
)
```

In later examples, it will be shown how you can extend the Template class to provide the parser with helpers and partials.

### Contextual templates

The *language* attribute can then be contextualised by executing the template:

```
tpl.execute({ language: "haxe" });
```

Apart from anonymous structures, you can also use class instances to populate a template. The result is just a string.

### Compile

Compile with `haxe build.hxml` in this directory to see the result.
