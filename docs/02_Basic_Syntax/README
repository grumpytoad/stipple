### Basic Syntax

This section will briefly cover features which should work identically to Handlebars. 

## Identifiers 

Lookups into nested variables in the context object are performed using a '.' or '/' delimited way:

```
{{get.my.nested.value}}
```

This will extract a context variable nested as follows:

```
{
  get: {
    my:  {
      nested: {
        value: "Found me!"
      }
    }
  }
}
```

## Block

A block syntax only gets executed if the expression resulting from the identifier lookup exists, is non-zero, and is not empty. Inverse blocks are executed if one of the reverse is true. In the former case a sub-context is passed to the block.

```
The weather in {{location}} is {{#weather}}cold and {{description}}{{^}}warm{{/weather}}
```

This will be compiled into "The weather in London is cold and filthy" if executed with:

```
{ 
  location: "London",
  weather: { 
    description: "filthy" 
  } 
}
```

## Helpers

Helpers are injectable callbacks into code that allow you to add more complex functionality, depending on the passed in context. Stipple does not have a concept of global helpers, they are sourced from a default set, and mixed into a locally scoped set.

```
The colours in the {{colorspace}} palette are {{#each colors}}{{.}}, {{/each}}
```

Would compile into "The colours in the RGB palette are red, green, blue, " if we use

```
{
  colorspace: "RGB",
  colors: [ "red", "green", "blue" ]
}
```

We show how to remove the final comma in the 'data' section.

## Partials

Partials are separate compiled template segments that can be referenced and executed with the passed in context. Stipple does not have a concept of global partials, they are always sourced from the local scope.

```
var inner = new Template().fromString("{{name}} is offering rooms from {{price}}.");

var outer = new Template().fromString("
The hotels in near {{location}} are:
{{#each hotels}}{{> "hotelTemplate" .}}{{/each}}");
```

The templates can be nested and included from different files. Each template will receive the immediate context of the previous template.

```
outer.execute({
  location: "London",
  [{
    name: "Hobbit's home",
    price: 0.2
  },
  {
    name: "Dragon's lair",
    price: "0.0"
  }]
  hotelTemplate: inner
});
```

## Data

The data node is a special '@'-syntax notation that can be passed in addition to the context from a helper function. For example, the "#each" helper populates the "@last" data node, if the helper has reached the last element in the passed-in context.

```
The colours in the {{colorspace}} palette are {{#each colors}}{{.}}{{#if @last}}{{^}}, {{/if}}{{/each}}
```
This template uses an additional "#if" helper to check the "@last" data node, and the "^" inverse block node which triggers on negation of the ternary condition.


```
{
  colorspace: "RGB",
  colors: [ "red", "green", "blue" ]
}
```

This compiles to "The colours in the RGB palette are red, green, blue".

More information on how to use the data node is provided when we delve into building helper methods.

## Comment

A special comment syntax exists for syntax that shouldn't be output at all. There is a short form and a long form.

```
{{! This is a comment}}
{{!-- This is also a comment --}}
```

## Escaping

If you need to output literal curly brackets instead of parsing them as a template token, you can use double backslashes:

```
\\\\{{not.tokenized}}
```

By default, the output of token expressions is html-escaped, if you do not want this, there are two token's available:

```
{{{not.escaped}}}
{{&not.escaped}}
```

If you want a helper method to not html-escape the output, you can use a special SafeString class:

```
{ 
  myhelper: function () { 
    return new stipple.SafeString("this-is-not-html-escaped"); 
  }
} 
```

